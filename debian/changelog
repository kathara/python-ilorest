python-ilorest (5.1.0.0+ds-1) unstable; urgency=medium

  * [33004df] New upstream version 5.1.0.0+ds
  * [033a439] d/control: Add myself to Uploaders
  * [d7de0b8] Rebuild patch queue from patch-queue branch
    Modified Patches:
    upstream/Correct-some-rst-syntax.patch
    upstream/Use-anonymous-reference-for-linking.patch

 -- Kathara Sasikumar <katharasasikumar007@gmail.com>  Sat, 29 Jun 2024 19:52:36 +0530

python-ilorest (5.0.0.0+ds-1) unstable; urgency=medium

  * [28fb1d0] New upstream version 5.0.0.0+ds
  * [1a0b54b] d/control: Bump Standards-Version to 4.7.0
    + No further changes needed.

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sun, 21 Apr 2024 08:29:06 +0200

python-ilorest (4.9.0.0+ds-1) unstable; urgency=medium

  * [1348f4a] New upstream version 4.9.0.0+ds

 -- Carsten Schoenert <c.schoenert@t-online.de>  Wed, 27 Mar 2024 19:47:44 +0100

python-ilorest (4.8.0.0+ds-1) unstable; urgency=medium

  * [f3e42d1] New upstream version 4.8.0.0+ds
  * [acdb704] Rebuild patch queue from patch-queue branch
    Added patch:
    upstream/ris.py-Use-raw-strings-for-all-regexes.patch

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sat, 03 Feb 2024 23:13:41 +0100

python-ilorest (4.7.0.0+ds-1) unstable; urgency=medium

  * [577fca9] New upstream version 4.7.0.0+ds

 -- Carsten Schoenert <c.schoenert@t-online.de>  Mon, 01 Jan 2024 11:43:48 +0100

python-ilorest (4.6.0.0+ds-1) unstable; urgency=medium

  * [94e69c7] New upstream version 4.6.0.0+ds

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sat, 28 Oct 2023 05:45:28 +0200

python-ilorest (4.5.0.0+ds-1) unstable; urgency=medium

  * [0ac53b3] d/watch: Add compression type
  * [300beed] New upstream version 4.5.0.0+ds

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sun, 01 Oct 2023 17:30:38 +0200

python-ilorest (4.3.0.0+ds-1) unstable; urgency=medium

  * [7e943b1] New upstream version 4.3.0.0+ds
  * [a0200c4] Revert "d/control: Add Built-Using to -doc package"
  * [15a0a0f] d/copyright: Update years of contributions
  * [804e1a8] d/watch: Add two more options

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sat, 01 Jul 2023 08:28:34 +0200

python-ilorest (4.2.0.1+ds-1) unstable; urgency=medium

  * [2391ab0] New upstream version 4.2.0.1+ds
  * [cbb8ace] d/control: Add Built-Using to -doc package

 -- Carsten Schoenert <c.schoenert@t-online.de>  Thu, 15 Jun 2023 19:18:31 +0200

python-ilorest (4.1.0.0+ds-1) experimental; urgency=medium

  * [885fbf3] d/{control,rules}: Move over to dh-sequence-python3
  * [4e2d278] d/{control,rules}: Move to dh-sequence-sphinxdoc
  * [91e20ca] d/*: Don't try running dh_auto_tests
  * [d6d2e6c] New upstream version 4.1.0.0+ds
  * [85aec86] d/control: Bump Standards-Version to 4.6.2
    + No further changes needed.

 -- Carsten Schoenert <c.schoenert@t-online.de>  Mon, 24 Apr 2023 20:35:31 +0200

python-ilorest (3.6.0.0+ds-1) unstable; urgency=medium

  * [c3f8634] New upstream version 3.6.0.0
  * [4c8c56e] Rebuild patch queue from patch-queue branch
    + removed patches:
      Remove-git-merge-cruft.patch
      debian-hacks/Adjust-import-of-collections-module.patch

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sun, 09 Oct 2022 10:26:50 +0200

python-ilorest (3.5.1.0+ds-1) unstable; urgency=medium

  * [f94ba78] New upstream version 3.5.1.0+ds
  * [bf1d6cf] d/control: Bump Standards-Version to 4.6.1
    + No further changes needed.
  * [d1f9c88] d/copyright: Update years of contributions

 -- Carsten Schoenert <c.schoenert@t-online.de>  Mon, 30 May 2022 20:00:39 +0200

python-ilorest (3.5.0.0+ds-1) unstable; urgency=medium

  * [bc6faf2] New upstream version 3.5.0.0+ds

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sun, 30 Jan 2022 16:19:45 +0100

python-ilorest (3.3.0.0+ds-1) unstable; urgency=medium

  * [cdd05ff] d/watch: Adjust watch logic again
  * [62848dc] New upstream version 3.3.0.0+ds
  * [61c50c4] Rebuild patch queue from patch-queue branch
    + added patch:
      Fix-up-usage-of-dash-separated-key-in-setup.cfg.patch
      debian-hacks/Adjust-import-of-collections-module.patch
    + modified patch:
      Correct-some-rst-syntax.patch
  * [b6e9d3a] d/control: Bump Standards-Version to 4.6.0
    + No further changes needed.

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sat, 11 Dec 2021 07:31:16 +0100

python-ilorest (3.2.2+ds-1) unstable; urgency=medium

  * [9e27c83] d/watch: Adjust watch logic
    + Upstream seems to rush the releases and is working a bit careless.
      The latest tag isn't containing the character 'v' in the release text,
      using 'git mode' now in uscan so it will detect new releases any way.
  * [64a9e6a] New upstream version 3.2.2+ds
  * [6e631b0] rebuild patch queue from patch-queue branch
    + added patch:
      debian-hacks/Make-examples-local-available.patch
      Remove-git-merge-cruft.patch
    + reworked patches (rebased):
      Add-missing-dependency-on-python-certifi.patch
      Correct-some-rst-syntax.patch
      Use-anonymous-reference-for-linking.patch
  * [3cc5925] python-ilorest-doc: Simplify sphinx installation stuff
    + Using dedicated override_dh_sphinxdoc that reduces the required sequencer
      files to a minimum. Also the amount of make targets in d/rules.
  * [12f37c6] python-ilorest-doc: Using sequencer file for examples
    + Using the option to let the examples sequencer defines the content to
      install.

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sun, 25 Jul 2021 17:32:34 +0200

python-ilorest (3.2.1+ds-2) unstable; urgency=medium

  * [14cc5a9] rebuild patch queue from patch-queue branch
    + added patches:
      Add-missing-dependency-on-python-certifi.patch

 -- Carsten Schoenert <c.schoenert@t-online.de>  Wed, 28 Apr 2021 19:04:59 +0200

python-ilorest (3.2.1+ds-1) unstable; urgency=medium

  * [a3e2e23] New upstream version 3.2.1+ds
  * [f30fe74] rebuild patch queue from patch-queue branch
   + removed patch (included upstream):
     resp_handler.py-check-verbosity-against-equality-of-1.patch
  * [8df6cd5] d/copyright: Update years of contributions

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sat, 24 Apr 2021 16:21:48 +0200

python-ilorest (3.1.1+ds-1) unstable; urgency=medium

  * [7bfe887] autopkg: mark current test superficial
    (Closes: #974508)
  * [38fb709] New upstream version 3.1.1+ds
    + Provided an option to input session_key for the REDfishClient class.
    + The iloaccounts command now provides the output in JSON format.
    + The createlogicaldrive quickdrive command now successfully runs.
    + BIOS and the poweron passwords can now be set without any password.
    + Fixed in this release:
      AHS data failing to download sometimes.
      An issue with downloading AHS when iLOREST is running locally on a
      server.
      The privilege modification of an iLO user account that was incorrectly
      applied on another user.
      The body of the onebuttonerase command, so that it could POST
      successfully.
  * [874c642] create patch queue from patchqueue branch
    + added patches:
      Correct-some-rst-syntax.patch
      Use-anonymous-reference-for-linking.patch
      resp_handler.py-check-verbosity-against-equality-of-1.patch
  * [b4c4532] d/control: bump Standards-Version to 4.5.1
    + No further changes needed.

 -- Carsten Schoenert <c.schoenert@t-online.de>  Tue, 17 Nov 2020 19:58:05 +0100

python-ilorest (3.1.0+ds-1) unstable; urgency=medium

  * [3b4e97e] New upstream version 3.1.0+ds
    + Added more examples
      * flash_firmware_by_uefi.py
      * reset_server.py
      * set_syslogserver.py
      * upload_firmware_ilo_repository_with_compsig.py
    + Updated to support Restful Interface Tool 3.1.0
  * [465cf0b] d/control: bump debhelper-compat to 13
  * [6c571cb] d/control: bump Standards-Version to 4.5.0
    + No further changes needed.
  * [e1bd3e8] d/u/metadata: adding upstream metadata file

 -- Carsten Schoenert <c.schoenert@t-online.de>  Wed, 07 Oct 2020 12:29:04 +0200

python-ilorest (3.0.0+ds-1) unstable; urgency=medium

  * [2e3bc11] New upstream version 3.0.0+ds
    + Added support for urllib3's kwargs. (timeout, retires, etc.)
    + Changed the format to create a Redfish or Rest client from a function
      into a Class.
    + Refactored v1 and rmc.
  * [4b17301] d/control: bump Standards-Version to 4.4.1
    + No further changes needed.
  * [3666c4f] doc-base: remove iLO_sys1.jpg from list
    + This file isn't shipped and used any more.
  * [8069353] dh: move over to debhelper-compat
  * [35a0046] d/control: add ${sphinxdoc:Depends}
  * [d8b2c92] d/control: adding Rules-Requires-Root: no

 -- Carsten Schoenert <c.schoenert@t-online.de>  Wed, 13 Nov 2019 22:25:24 +0100

python-ilorest (2.5.2+ds-1) unstable; urgency=medium

  * [4a2e3b3] New upstream version 2.5.2+ds

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sun, 15 Sep 2019 11:29:31 +0200

python-ilorest (2.5.1+ds-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Removed Python 2 support (Closes: #937829).

 -- Thomas Goirand <zigo@debian.org>  Sun, 08 Sep 2019 13:56:55 +0200

python-ilorest (2.5.1+ds-1) unstable; urgency=medium

  * [bb0334c] New upstream version 2.5.1+ds

 -- Carsten Schoenert <c.schoenert@t-online.de>  Tue, 16 Jul 2019 10:01:09 -0300

python-ilorest (2.5.0+ds-1) unstable; urgency=medium

  * [bca0381] New upstream version 2.5.0+ds
    + Added http/socks proxy support
  * [8b85452] d/rules: enable library tests while build
  * [07cb570] d/control: bump Standards-Version to 4.4.0
    + No further changes needed.

 -- Carsten Schoenert <c.schoenert@t-online.de>  Thu, 11 Jul 2019 12:19:54 +0200

python-ilorest (2.4.2+ds-1) unstable; urgency=medium

  * [9052da4] New upstream version 2.4.2+ds
    + Fixed error using urllib3
      see: https://github.com/HewlettPackard/python-ilorest-library/issues/46
    + Updated ex48_set_bios_password

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sun, 19 May 2019 09:59:33 +0200

python-ilorest (2.4.1-1) unstable; urgency=medium

  * [a87a094] New upstream version 2.4.1
    + Updates to use urlib3
    + Updates to reflect generic Redfish implementations in RMC
    + Improved validation code
  * [cc0519e] d/control: bump Standards-Version to 4.3.0
    + No further changes needed.

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sat, 30 Mar 2019 08:05:37 +0100

python-ilorest (2.3.1+20180725+ds-1) unstable; urgency=medium

  * initial upload (Closes: #903953)
  * []bbdb4b] New upstream version 2.3.1+20180725+ds
  * [7f38742] basic Debianization
  * [05cc4b5] adding a basic autopkgtest

 -- Carsten Schoenert <c.schoenert@t-online.de>  Wed, 25 Jul 2018 12:20:50 +0800
